<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE urls SYSTEM "entitites.dtd" [ 
    <!ENTITY mmdr "http://purl.org/momayo/mmdr/">
   ]>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"   
    xmlns:bibo="http://purl.org/ontology/bibo/"   
    xmlns:dct="http://purl.org/dc/terms/"
    xmlns:mmdr="&mmdr;"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" 
    xmlns:flub="http://ub.uib.no/flibub"    
    
    exclude-result-prefixes="xs flub" version="2.0">
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 11, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> ogj077</xd:p>
            <xd:p>Stylesheet for Transforming a file-list XML into triples for Digital-resources,
                aggregations and pages. Takes a result file from createfilelist.xsl (on our digital
                resources) and creates an RDF representation. </xd:p>
            <xd:p>Creates mmdrDigitalResource, ore:Aggregation, a classless Inverse document
                relation (isResourceOf, isPartOf), mmdrPage </xd:p>
            <xd:p>A sparql xml result file for s? dct:identifier ?identifier is used for connecting
                ?s to the digital resources.</xd:p>
            <xd:p>@change: make RNG for file XML type</xd:p>
            <xd:p>@change: Should we use more specific types on parameters for templates? </xd:p>
            <xd:p>@change: Also introduce som named templates? (Enforce correct parameter names etc
                for passing)</xd:p>
        </xd:desc>
    </xd:doc>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>boolean switch for more verbose messaging.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="debug" as="xs:boolean" select="false()"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>Sparql result file (or protege project itself) used to find connections between DR
                and images.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="lookup"
        select="'../test.xml'"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>parameter of current base-uri for files, to simplify reuse on DR model for
                different URIs.</xd:p>
        </xd:desc>
    </xd:doc>
 <!--   <xsl:param name="base-file-resource-uri" select="'&dzidloc;'"/>-->
    <!--<xsl:include href="../../common/felles.xsl"/>
-->
    <!-- global keys-->
    <xsl:key name="page" use="*:file/@page" match="*:page"/>
    <xsl:key name="page" use="@page" match="*:page"/>   
    <xsl:key name="lookup_identifier" match="*" use="dct:identifier/replace(text(),$rep_regex,'$1')"/>
    <xsl:key name="lookup_identifier" match="*[matches(dct:identifier[1],'^ska|ubb-ska','i')]" use="dct:identifier/flub:ska-identifier(text())"/>
    
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <!--global variables-->
    <xsl:variable name="rep_regex" select="'-([a-z])$'"/>       
    <xsl:variable name="lookup_base" select="document($lookup)"/>
    <xsl:variable name="base-file-resource-uri" select="'&dzidloc;'"/>
    <xsl:variable name="self" select="/"/>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>Create namespaces, and apply signature elements with mode document.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:if test="$debug">
            <xsl:message>/</xsl:message>
        </xsl:if>
        <xsl:element name="rdf:RDF">   
            <xsl:namespace name="bibo" select="'http://purl.org/ontology/bibo/'"/>
            <xsl:namespace name="dct" select="'http://purl.org/dc/terms/'"/>
            <xsl:namespace name="mmdr" select="'&mmdr;'"/>
            <xsl:namespace name="ore" select="'http://www.openarchives.org/ore/terms/'"/>
            <xsl:namespace name="rdf" select="'http://www.w3.org/1999/02/22-rdf-syntax-ns#'"/>
            <xsl:namespace name="rdfs" select="'http://www.w3.org/2000/01/rdf-schema#'"/>            
       
            <xsl:apply-templates select="descendant::*:signature" mode="document"/>
        </xsl:element>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>Ignore signature 'filestamp' elements.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="*:signature[@n = 'filestamp']" mode="document"/>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>When in signature mode Document, apply only first file element, for setting up
                context for the rest of the transformation.</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="*:signature" mode="document">
        <xsl:if test="$debug">
            <xsl:message>*signature document mode </xsl:message>
        </xsl:if>
        <xsl:apply-templates select="descendant::*:file[1]" mode="document"/>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>@mode=document starting point for each unique @n /identifier in file list, starts
                all other transformations</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:template match="*:file" mode="document">
        <xsl:if test="$debug">
            <xsl:message>file document</xsl:message>
        </xsl:if>
        <xsl:variable name="documents" as="element(*)*">
            <xsl:call-template name="get-subjects-from-Signature">
                <xsl:with-param name="lookupbase" select="$lookup_base"/>
                <xsl:with-param name="signature" select="@n"/>
            </xsl:call-template>
        </xsl:variable>
        <!-- To use this @n in xpath expressions-->
        <xsl:variable name="signatur" select="@n"/>

        <!--copies element type and rdf about from the lookup base-->
        <xsl:variable name="ore:Aggregation" select="flub:catPrefixURI('&aggregation;', $signatur)"/>
        <xsl:variable name="pages" as="node()*"
            select="(ancestor::signature/descendant::page)"/>

        <xsl:variable name="images" as="node()*" select="self::*[not(string(@page)) and not(@filetype='pdf')]"/>
        <!-- all page URIs for mmdr:hasItem-->
        <xsl:variable name="pages_uris" as="xs:string*">
            <xsl:for-each select="$pages">                
                <xsl:sequence select="flub:setPageURIFromPage(ancestor-or-self::page)"/>
            </xsl:for-each>
        </xsl:variable>
        <!-- To use this @page in xpath expr-->
        <xsl:variable name="page"
            select="(ancestor-or-self::*:page/@page, ancestor::*:signature/descendant::*:page[@page]/@page[1], ancestor::*:signature/descendant::*:file[parent::*:page and @page]/@page[1])[1]"/>

        <!-- first pdf file in same signature-->
        <xsl:variable name="pdf"
            select="(ancestor::signature/descendant::*[@filetype = 'pdf' and @n = $signatur])[1]"/>
     <xsl:if test="exists($pdf/self::*)">
         <xsl:apply-templates mode="mmdr:DigitalResource" select="$pdf">
            <xsl:with-param name="mmdr:isResourceOf" select="$ore:Aggregation"/>
            <xsl:with-param name="mmdr:DigitalResource" select="flub:catPrefixURI('&dr;', @n)"/>
            <xsl:with-param name="rdfs:label" select="'Pdf'"/>
            <!--@change: hardcoded debth on filesystem to uri conversion.-->
            <xsl:with-param name="mmdr:hasView"
                select="flub:writeHasViewURI($pdf/@path)"
            />
        </xsl:apply-templates>
     </xsl:if>
        <xsl:variable name="secondOrFirstPage"
            select="(key('page', $page)/descendant-or-self::*:file[contains(@path, '_th.jpg')][1]/@path, ancestor::*:signature//descendant::*:file[contains(@path, '_th.jpg')][1]/@path)[1]"/>
        
        <xsl:if test="not($secondOrFirstPage) and string($page)">
            <xsl:message> page key or item thumbnail not found !!! PAGE<xsl:value-of select="$page"
                /> IDENTIFIER<xsl:sequence select="$signatur"/>
            </xsl:message>
        </xsl:if>
        <!-- Create mmdr:Canvas-->
        <xsl:apply-templates select="$pages" mode="mmdr:Canvas">
            <xsl:with-param name="mmdr:isItemOf" select="$ore:Aggregation"/>
        </xsl:apply-templates>

        <!-- Create ore:Aggregation-->
        <xsl:apply-templates select="self::node()" mode="ore:Aggregation">
            <xsl:with-param name="ore:Aggregation" select="$ore:Aggregation"/>
            <xsl:with-param name="mmdr:hasItem"
                select="
                    $pages_uris,
                    if (not(@page) and not(matches(@filetype,'pdf'))) then
                        flub:catPrefixURI('&dr;', @n)
                    else
                        ''"/>
            <xsl:with-param name="mmdr:hasResource"
                select="
                    if ($pdf) then
                        flub:catPrefixURI('&dr;', @n)
                    else
                        ()"/>
            <xsl:with-param name="mmdr:isRepresentationOf" select="$documents/@rdf:about"/>
            <xsl:with-param name="mmdr:hasThumbnail"
                select="if ($secondOrFirstPage[string(.)])
                then flub:writeHasViewURI($secondOrFirstPage[1]) else ()"/>
        </xsl:apply-templates>
        <!-- create bibo:Document instance-->
        <xsl:if test="exists($documents)">
            <xsl:apply-templates select="self::node()" mode="bibo:Document">
                <xsl:with-param name="bibo:pages" select="count((ancestor::signature/page))"/>
                <xsl:with-param name="bibo:Document" select="$documents"/>
                <xsl:with-param name="mmdr:hasRepresentation" select="$ore:Aggregation"/>
            </xsl:apply-templates>

            <!-- Create bibo:owner (Static to ubb) -->

            <!-- fires off start step for digitalresources, first of each uniqe @page attribute,
                to create aggregations on digitalresources
                Each step from here has -->
        </xsl:if>
        <xsl:apply-templates select="$pages" mode="mmdr:DigitalResources"/>
        <xsl:apply-templates select="$images" mode="mmdr:DigitalResources"/>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>This bibo:Document has no metadata, only a mmdr for connecting files and
                identifier</xd:p>
        </xd:desc>
        <xd:param name="bibo:Document">bibo:Document is to be url of document, most often a looked
            up matched URI, as a string</xd:param>
        <xd:param name="mmdr:hasRepresentation">mmdr:hasRepresentation is a sequence of strings
            that should be passed URIs for each represantion</xd:param>
        <xd:param name="bibo:pages">bibo:pages is counted based on number page elements
            selected</xd:param>
    </xd:doc>
    
    <!--generisk predikat pna:hasThumbnail, er denne fortsatt i bruk? la til mmdr:hasThumbnail-->
    <xsl:template match="@*|*" mode="mmdr:hasThumbnail">     
        <xsl:param name="mmdr:hasThumbnail"/>
        <mmdr:hasThumbnail rdf:datatype="{concat('&xsd;','anyuri')}">
            <xsl:value-of select="$mmdr:hasThumbnail"/>
        </mmdr:hasThumbnail>
    </xsl:template>
    
    <xsl:template match="*|@*" mode="rdfs:label">
        <xsl:param name="rdfs:label" as="xs:string" select="''"/>
        <xsl:param name="lang" select="''"/>
        <xsl:element name="rdfs:label">
            <xsl:attribute name="rdf:datatype" select="'http://www.w3.org/2001/XMLSchema#string'"/>            
            <xsl:if test="string-length($lang) > 0"><xsl:attribute name="xml:lang" select="$lang"></xsl:attribute> </xsl:if> 
            <xsl:value-of select="normalize-space(($rdfs:label[string(.)],.)[1])"/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="*" mode="bibo:Document">
        <xsl:param name="bibo:Document" as="element(*)*"/>
        <xsl:param name="mmdr:hasRepresentation" as="xs:string*"/>
        <xsl:param name="bibo:pages"/>
        
        <xsl:variable name="this.node" select="self::node()"/>
        <xsl:for-each select="$bibo:Document">
        <xsl:element name="rdf:Description">
            <xsl:attribute name="rdf:about" select="@rdf:about"/>

            <xsl:if test="($bibo:pages > 1)">
                <bibo:pages rdf:datatype="http://www.w3.org/2001/XMLSchema#int">
                    <xsl:value-of select="$bibo:pages"/>
                </bibo:pages>
            </xsl:if>

            <xsl:apply-templates select="self::node()" mode="mmdr:hasRepresentation">
                <xsl:with-param name="mmdr:hasRepresentation" select="$mmdr:hasRepresentation"/>
            </xsl:apply-templates>

            <xsl:variable name="n" select="$this.node/@n"/>
            <!--looks for second or first page that has a _th (thumbnail file) in @path.-->
            <xsl:variable name="secondOrFirstPage"
                select="$this.node/parent::*/descendant-or-self::*[(@page or @n) and matches(@path, '_th')][1]/@path"/>
            <xsl:if test="$secondOrFirstPage">
                <xsl:apply-templates select="$this.node" mode="mmdr:hasThumbnail">
                    <xsl:with-param name="mmdr:hasThumbnail"
                        select="flub:writeHasViewURI($secondOrFirstPage[1])"/>
                </xsl:apply-templates>
            </xsl:if>
        </xsl:element>
        </xsl:for-each>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>for combining hasResource, hasPart and hasThumbnail</xd:p>
        </xd:desc>
        <xd:param name="ore:Aggregation">Aggregation URI/Name for ore:Aggregation</xd:param>
        <xd:param name="mmdr:hasItem">string* with URIs to pages or Parts of the documents. Also used
            for images, since a page may have negative, positive etc. versions on the
            webpage.</xd:param>
        <xd:param name="mmdr:hasResource">Resource, web or otherwise representing the item in full
            (for instance PDF or XML file of entire document</xd:param>
        <xd:param name="mmdr:hasThumbnail">URI to a thumbnail image</xd:param>
        <xd:param name="mmdr:isRepresentationOf"> URI of aggregation owner</xd:param>
    </xd:doc>   
    <xsl:template match="*" mode="ore:Aggregation">
        <xsl:param name="ore:Aggregation"/>
        <xsl:param name="mmdr:hasItem" as="xs:string*"/>
        <xsl:param name="mmdr:hasResource"/>
        <xsl:param name="mmdr:hasThumbnail"/>
        <xsl:param name="mmdr:isRepresentationOf" as="xs:string*"/>
        <xsl:variable name="self" select="."/>
        <rdf:Description rdf:about="{$ore:Aggregation}">
            <rdf:type rdf:resource="&ore;Aggregation"/>
            <xsl:for-each select="$mmdr:hasItem">
                <xsl:apply-templates select="$self" mode="mmdr:hasItem">
                    <xsl:with-param name="mmdr:hasItem" select="."/>
                </xsl:apply-templates>
            </xsl:for-each>
            <xsl:apply-templates select="self::node()/@n" mode="rdfs:label"/>
            <xsl:apply-templates select="self::node()" mode="mmdr:hasResource">
                <xsl:with-param name="mmdr:hasResource" select="$mmdr:hasResource"/>
            </xsl:apply-templates>
            <xsl:if test="$mmdr:hasThumbnail">
                <xsl:apply-templates select="self::node()" mode="mmdr:hasThumbnail">
                    <xsl:with-param name="mmdr:hasThumbnail" select="$mmdr:hasThumbnail"/>
                </xsl:apply-templates>
            </xsl:if>
            <xsl:apply-templates select="self::node()" mode="mmdr:isRepresentationOf">
                <xsl:with-param name="mmdr:isRepresentationOf" select="$mmdr:isRepresentationOf"
                />
            </xsl:apply-templates>
        </rdf:Description>
    </xsl:template>

    <!-- 
    @param mmdr:isItemOf URI to ore:Aggregation -->
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>mmdr:Canvas</xd:p>
        </xd:desc>
        <xd:param name="mmdr:isItemOf"/>
    </xd:doc>
    <xsl:template match="*" mode="mmdr:Canvas">
        <xsl:param name="mmdr:isItemOf" as="xs:string"/>
        <xsl:variable name="mmdr:hasThumbnail">
            <xsl:variable name="path-attribute" select="ancestor-or-self::page/descendant::*[contains(@path, '_th')][1]/@path[1]"/>
            <xsl:value-of
                select="if ($path-attribute[string(.)]) then flub:writeHasViewURI($path-attribute) else ()"
            />
        </xsl:variable>
        <xsl:variable name="pos" select="position()"/>
        <xsl:variable name="signatur" select="@n"/>
        <xsl:variable name="prev"
            select="ancestor-or-self::page/preceding-sibling::page[1]"/>
        <xsl:variable name="next"
            select="ancestor-or-self::page/following-sibling::page[1]"/>

        <rdf:Description rdf:about="{flub:setPageURIFromPage(ancestor-or-self::page[1])}">
         
            <xsl:if test="$mmdr:hasThumbnail[string(.)]">
                <mmdr:hasThumbnail>
                <xsl:value-of select="$mmdr:hasThumbnail"/>
            </mmdr:hasThumbnail>
            </xsl:if>
           
            <xsl:variable name="previousCanvas"
                select="
                    if ($prev) then
                        flub:setPageURIFromPage($prev/ancestor-or-self::page)
                    else
                        ()"/>
            <xsl:variable name="nextCanvas"
                select="
                    if ($next) then
                        flub:setPageURIFromPage($next/ancestor-or-self::page)
                    else
                        ()"/>
            <rdf:type rdf:resource="&mmdr;Canvas"/>

            <xsl:apply-templates mode="mmdr:isItemOf" select="self::node()">
                <xsl:with-param name="mmdr:isItemOf" select="$mmdr:isItemOf"/>
            </xsl:apply-templates>
            <xsl:if test="exists($previousCanvas)">
                <xsl:apply-templates mode="mmdr:previousCanvas" select="self::node()">
                    <xsl:with-param name="mmdr:previousCanvas" select="$previousCanvas"/>
                </xsl:apply-templates>
            </xsl:if>

            <xsl:if test="exists($nextCanvas)">
                <xsl:apply-templates mode="mmdr:nextCanvas" select="self::node()">
                    <xsl:with-param name="mmdr:nextCanvas" select="$nextCanvas"/>
                </xsl:apply-templates>
            </xsl:if>

            <!--mode @mmdr:sequenceNr calculates position for sequencenumber-->
            <xsl:apply-templates select="self::node()" mode="mmdr:sequenceNr">
                <xsl:with-param name="mmdr:sequenceNr" select="position() cast as xs:int"/>
            </xsl:apply-templates>
            <!-- concats a label for page "side X"-->
            <xsl:apply-templates select="self::node()" mode="rdfs:label">
                <xsl:with-param name="rdfs:label" select="concat('side ', position())"/>
            </xsl:apply-templates>
            <!-- @change: Only single hasResource? -->
            <xsl:apply-templates select="self::node()" mode="mmdr:hasResource">
                <xsl:with-param name="mmdr:hasResource" select="flub:catPrefixURI('&dr;', flub:getPosStringFromPage(self::page))"
                />
            </xsl:apply-templates>
        </rdf:Description>
    </xsl:template>

    <!--mmdr:hasResource @change: Move to felles.xsl-->
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p/>
        </xd:desc>
        <xd:param name="mmdr:hasResource"/>
    </xd:doc>
    <xsl:template match="*" mode="mmdr:hasResource">
        <xsl:param name="mmdr:hasResource"/>
        <xsl:if test="string($mmdr:hasResource)">
            <mmdr:hasResource rdf:resource="{$mmdr:hasResource}"/>
        </xsl:if>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>@handling of view from point of First image file. Creates a sequence of URIs thats
                passed to mmdr:hasView mode</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="*:page|*:file" mode="mmdr:DigitalResources">
        <xsl:variable name="page-attrib" select="@page"/>
        <xsl:variable name="n-attrib" select="ancestor-or-self::*[@n][1]/@n"/>
        <xsl:variable name="mmdr:hasView" as="xs:string*">
            <xsl:for-each
                select="descendant-or-self::*:file[(flub:getIntegerPageOrZero(@page) = flub:getIntegerPageOrZero($page-attrib) or (not(exists(@page)))) and @n = $n-attrib],following-sibling::*:file,preceding-sibling::file">
               <xsl:if test="not(matches(@path,'.pdf'))">
                <xsl:sequence select="flub:writeHasViewURI(@path)"/>
               </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:apply-templates select="self::node()" mode="mmdr:DigitalResource">
            <xsl:with-param name="mmdr:hasView" select="$mmdr:hasView"/>
            <xsl:with-param name="mmdr:DigitalResource"
                select="flub:catPrefixURI('&dr;', if (self::page) then (flub:getPosStringFromPage(self::page) )else  $n-attrib)"/>
            <xsl:with-param name="mmdr:isResourceOf"
                select="
                    if (@page) 
                    then flub:setPageURIFromPage(ancestor-or-self::page)
                    else flub:catPrefixURI('&aggregation;', $n-attrib)"
            />
        </xsl:apply-templates>
    </xsl:template>

    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl">
        <xd:desc>
            <xd:p>overriding felles.xsl</xd:p>
        </xd:desc>
        <xd:param name="mmdr:DigitalResource"/>
        <xd:param name="mmdr:hasView"/>
        <xd:param name="mmdr:isRepresentationOf"/>
        <xd:param name="mmdr:isResourceOf"/>
        <xd:param name="rdfs:label"/>
    </xd:doc>
    <xsl:template match="*" mode="mmdr:DigitalResource" priority="3.0">
        <xsl:param name="mmdr:DigitalResource"/>
        <xsl:param name="mmdr:hasView"/>
        <xsl:param name="mmdr:isResourceOf"/>
        <xsl:param name="rdfs:label"/>

        <rdf:Description rdf:about="{$mmdr:DigitalResource}">
            <rdf:type rdf:resource="&mmdr;DigitalResource"/>

            <xsl:apply-templates select="self::node()" mode="mmdr:hasView">
                <xsl:with-param name="mmdr:hasView" select="$mmdr:hasView"/>
            </xsl:apply-templates>

            <xsl:apply-templates select="self::node()" mode="mmdr:isResourceOf">
                <xsl:with-param name="mmdr:isResourceOf" select="$mmdr:isResourceOf"/>
            </xsl:apply-templates>

            <xsl:if test="string($rdfs:label)">
                <xsl:apply-templates select="self::node()" mode="rdfs:label">
                    <xsl:with-param name="rdfs:label" select="$rdfs:label"/>
                </xsl:apply-templates>
            </xsl:if>

            <xsl:variable name="last-modified"
                select="
                    if (@modified) then
                        @modified
                    else
                        max(for $x in ancestor-or-self::signature/descendant::*/@modified
                        return
                            xs:dateTime($x))"
                as="xs:dateTime?"/>
            <xsl:if test="exists($last-modified)">
                <dct:modified rdf:datatype="http://www.w3.org/2001/XMLSchema#dateTime">
                    <xsl:value-of select="$last-modified"/>
                </dct:modified>
            </xsl:if>
        </rdf:Description>
    </xsl:template>
    
    <!--generisk predikat mmdr:hasRepresentation-->
    <xsl:template match="*" mode="mmdr:hasRepresentation">
        <xsl:param name="mmdr:hasRepresentation" as="xs:string*"/>
        <xsl:for-each select="$mmdr:hasRepresentation">
            <mmdr:hasRepresentation rdf:resource="{.}"/>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:hasView">
        <xsl:param name="mmdr:hasView" as="xs:string*"/>
        <xsl:for-each select="$mmdr:hasView[string(.)]">
            <xsl:choose>    
                <xsl:when test="matches(.,'Dzi/.+?\.(xml|dzi)$','i')">
                    <mmdr:hasDZIView><xsl:value-of select="."/></mmdr:hasDZIView>
                </xsl:when>
                <xsl:when test="matches(.,'_th.jpg$','i')">
                    <mmdr:hasTHView><xsl:value-of select="."/></mmdr:hasTHView>
                </xsl:when>      
                <xsl:when test="matches(.,'_sm.jpg$','i')">
                    <mmdr:hasSMView><xsl:value-of select="."/></mmdr:hasSMView>
                </xsl:when>      
                <xsl:when test="matches(.,'_md.jpg$','i')">
                    <mmdr:hasMDView><xsl:value-of select="."/></mmdr:hasMDView>
                </xsl:when>
                <xsl:when test="matches(.,'_lg.jpg$','i')">
                    <mmdr:hasLGView><xsl:value-of select="."/></mmdr:hasLGView>
                </xsl:when>
                <xsl:when test="matches(.,'_xl.jpg$','i')">
                    <mmdr:hasXLView><xsl:value-of select="."/></mmdr:hasXLView>
                </xsl:when>
                <xsl:when test="matches(.,'_xs.jpg$','i')">
                    <mmdr:hasXSView><xsl:value-of select="."/></mmdr:hasXSView>
                </xsl:when>      
                <xsl:when test="matches(.,'.pdf')"><mmdr:hasView><xsl:value-of select="."/></mmdr:hasView></xsl:when>
            </xsl:choose>            
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:hasItem">
        <xsl:param name="mmdr:hasItem" as="xs:string*"/>
        <xsl:param name="mmdr:isItemOf"/>    
        <xsl:for-each select="$mmdr:hasItem[string(.)]">
            <xsl:choose>
                <xsl:when test="string($mmdr:isItemOf)">
                    <mmdr:hasItem>
                        <rdf:Description rdf:about="{.}">
                            <mmdr:isItemOf rdf:resource="{$mmdr:isItemOf}"/>
                        </rdf:Description>
                    </mmdr:hasItem>
                </xsl:when>
                <xsl:otherwise>
                    <mmdr:hasItem rdf:resource="{.}"/>
                </xsl:otherwise>
            </xsl:choose>      
        </xsl:for-each>    
    </xsl:template>
    
    <!--generisk predikat til forrige side-->
    <xsl:template match="*|@*" mode="mmdr:previousCanvas">
        <xsl:param name="mmdr:previousCanvas"/>
        <xsl:if test="string($mmdr:previousCanvas)">
            <mmdr:previousCanvas rdf:resource="{$mmdr:previousCanvas}"/>
        </xsl:if>
    </xsl:template>
    
    <!--generisk predikat til neste side-->
    <xsl:template match="*|@*" mode="mmdr:nextCanvas">
        <xsl:param name="mmdr:nextCanvas"/>
        <xsl:if test="not(matches(($mmdr:nextCanvas,.)[1],'segl','i'))">
            <mmdr:nextCanvas rdf:resource="{$mmdr:nextCanvas}"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:isResourceOf">
        <xsl:param name="mmdr:isResourceOf"/>
        <xsl:if test="string($mmdr:isResourceOf)">
            <mmdr:isResourceOf rdf:resource="{$mmdr:isResourceOf}"/>
        </xsl:if>
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:sequenceNr">
        <xsl:param name="mmdr:sequenceNr" as="xs:integer"/>
        <mmdr:sequenceNr rdf:datatype="&xsd;integer">
            <xsl:value-of select="$mmdr:sequenceNr"/>
        </mmdr:sequenceNr>
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:isRepresentationOf">
        <xsl:param name="mmdr:isRepresentationOf"/>
        <xsl:param name="mmdr:hasRepresentation"/>     
        <xsl:choose>
            <xsl:when test="not(exists($mmdr:isRepresentationOf))"/>
            <xsl:when test="string($mmdr:hasRepresentation)">                
                <xsl:for-each select="$mmdr:isRepresentationOf">
                    <mmdr:isRepresentationOf>
                        <rdf:Description rdf:about="{.}">
                            <mmdr:hasRepresentation rdf:resource="{$mmdr:hasRepresentation}"/>
                        </rdf:Description>
                    </mmdr:isRepresentationOf>
                </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$mmdr:isRepresentationOf">
                    <mmdr:isRepresentationOf rdf:resource="{.}"/>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>    
    </xsl:template>
    
    <xsl:template match="*" mode="mmdr:isItemOf">
        <xsl:param name="mmdr:isItemOf"/>
        <xsl:param name="rdf:type" />   
        <xsl:param name="class" select="'exists'"/>
        <xsl:param name="label"/>
        <xsl:choose>
            <xsl:when test="$class ='exists'">        
                <mmdr:isItemOf rdf:resource="{$mmdr:isItemOf}"/>
            </xsl:when>
            <xsl:otherwise>                
                <xsl:element name="mmdr:isItemOf">
                    <xsl:element name="rdf:Description">
                        <xsl:attribute name="rdf:type" select="$class"/>            
                        <xsl:attribute name="rdf:about" select="$mmdr:isItemOf"/>
                        <xsl:apply-templates select="self::node()" mode="rdfs:label">
                            <xsl:with-param name="label" select="$label"/>
                        </xsl:apply-templates>
                        <rdfs:comment>mmdr</rdfs:comment>
                    </xsl:element>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- functions-->    
    <xsl:function name="flub:ska-identifier">
        <xsl:param name="signature"/>
        <xsl:sequence select="replace(replace($signature,'^ska-','ubb-ska-','i'),'^(ubb-ska-[0-9]{4}-[a-z]-)0?([0-9]-)0?([1-9]?)','$1$2$3')"/>
    </xsl:function> 
    
    <xsl:function name="flub:setPageURIFromPage">
        <xsl:param name="page" as="element(page)?"/>            
        <xsl:variable name="thisPageString"
            select="flub:getPosStringFromPage($page)"/>
        <xsl:value-of select="flub:catPrefixURI('&pagerdf;', $thisPageString)"/>
    </xsl:function>
    
    <xsl:function name="flub:getPosStringFromPage">
        <xsl:param name="page" as="element(page)"/>
        <xsl:variable name="signature" select="$page/ancestor-or-self::*/@n[1]" as="xs:string"/>
        <xsl:variable name="pos" select="count($page/preceding-sibling::page) + 1" as="xs:integer"/>
        <xsl:variable name="length" select="string-length(string(count(key('page',$page,$self)/page)))"/>
        <xsl:variable name="format" select="for $x in 1 to $length return '0'" as="xs:string*"/>        
        <xsl:value-of select="concat($signature, '_p',format-number($pos, string-join($format,'')))"/>        
    </xsl:function>
    
    <xsl:function name="flub:writeHasViewURI">
        <xsl:param name="path" as="attribute(path)"/>
        <xsl:if test="string($path)">
            <xsl:value-of select="flub:writeHasViewURI($path,4)"/>
        </xsl:if>
    </xsl:function>
    
    <!--function to write a partial path (with debth selected)--> 
    <xsl:function name="flub:writeHasViewURI">
        <xsl:param name="path" as="attribute(path)"/>
        <xsl:param name="depth" as="xs:integer"/>
        <xsl:variable name="base-file-resource-uri" select="($base-file-resource-uri,'&dzidloc;')[1]"/>
        <xsl:if test="count(tokenize($path,'/')) &lt; ($depth,4)[1]">
            <xsl:message terminate="yes">flub:writeHasViewURI out of bounds.</xsl:message>
        </xsl:if>
        <xsl:value-of
            select="concat($base-file-resource-uri,string-join( for $x in tokenize($path,'/')[position()=($depth,4)[1] to last()] return encode-for-uri($x) ,'/'))"
        />
    </xsl:function>    
    
    <xsl:function name="flub:getIntegerPageOrZero" as="xs:integer">
        <xsl:param name="page"/>
        <xsl:variable name="pageNumberOrString" select="replace($page,'^.+_p0*','')"/>
        <xsl:sequence select="if ($pageNumberOrString castable as xs:integer) then xs:integer($pageNumberOrString) else 0"/>
    </xsl:function>
    
    <xsl:function name="flub:catPrefixURI" as="xs:string">
        <xsl:param name="prefix" as="xs:string"/>
        <xsl:param name="slug" as="xs:string"/>
        <xsl:if test="not(string($slug))">
            <xsl:message>flub:catPrefixURI: URI NOT FOUND</xsl:message>
        </xsl:if>
        <xsl:value-of select="concat($prefix,encode-for-uri($slug))"/>
    </xsl:function>
    
    <!-- named templates-->
    <xsl:template name="get-subjects-from-Signature">
        <xsl:param name="signature" as="xs:string"/>
        <xsl:param name="lookupbase" as="node()*"/>
        <xsl:variable name="identifier-bySignature">        
            <xsl:choose>
                <xsl:when test="matches($signature,'^ska|ubb-ska')">
                    <xsl:value-of select="flub:ska-identifier(replace($signature,$rep_regex,'$1'))"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="replace($signature,$rep_regex,'$1')"/>
                </xsl:otherwise>
            </xsl:choose>            
        </xsl:variable>
        <xsl:for-each select="$lookupbase">
            <xsl:sequence select="key('lookup_identifier',$identifier-bySignature)"/>
        </xsl:for-each>        
    </xsl:template>
    
   
</xsl:stylesheet>
